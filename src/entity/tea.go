package entity

import (
	"net/url"
	"gopkg.in/mgo.v2/bson"
)

// Default test entity
type Tea struct {
	// Primary key of entity
	Id    bson.ObjectId `json:"id, omitempty" bson:"_id"`
	Title string        `json:"title"`
	Type  TeaType       `json:"type"`
}

func (t *Tea) SetId(id string) {
	t.Id = bson.ObjectId(id)
}

func (t *Tea) GetId() string {
	return t.Id.Hex()
}

func ParseTea(v url.Values) *Tea {
	tea := new(Tea)
	tea.SetId(v.Get("id"))
	tea.Title = v.Get("title")
	tea.Type = TeaType(v.Get("type"))
	return tea
}

func DefaultTea() *Tea {
	return &Tea{"f2a4afc3e", "Some Tea", BlackTea}
}

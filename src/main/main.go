package main

import (
	"log"
	"net/http"
	"dynamic-web/src/web"
	"time"
)

func main() {
	server := http.Server{
		Handler:        web.Router,
		Addr:           ":8080",
		MaxHeaderBytes: 2 << 12,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}
	log.Fatal(server.ListenAndServe())
}

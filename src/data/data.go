package data

import "gopkg.in/mgo.v2"

const connectionUrl = "mongodb://127.0.0.1"
const db = "test"

type data struct {
	db string
}

func (d data) getConnection() (*mgo.Database, error) {
	session, err := mgo.Dial(connectionUrl)
	if err != nil {
		return nil, err
	}
	return session.DB(d.db), nil
}

func (d data) GetCollection(name string) (*mgo.Collection, error) {
	if con, err := d.getConnection(); err != nil {
		return nil, err
	} else {
		return con.C(name), err
	}
}

func newData() data {
	return data{db: db}
}

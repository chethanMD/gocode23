package web

import (
	"github.com/gorilla/mux"
)

var Router = mux.NewRouter()

func init() {
	r := Router
	r.HandleFunc("/tea/{id}", teaHandler)
	r.HandleFunc("/tea-create", createTeaHandler).Methods("POST")
	r.HandleFunc("/tea-edit", editTeaHandler).Methods("POST")
	r.HandleFunc("/", indexHandler).Methods("GET")
}

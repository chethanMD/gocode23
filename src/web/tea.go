package web

import (
	"dynamic-web/src/entity"
	"dynamic-web/src/service"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"gopkg.in/mgo.v2/bson"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	parseTemplates(w, func(t *template.Template) {
		result, err := service.GetAll()
		checkError(w, http.StatusInternalServerError, err, func() { t.ExecuteTemplate(w, "index", result) })
	})
}

func teaHandler(w http.ResponseWriter, r *http.Request) {
	parseTemplates(w, func(t *template.Template) {
		id := mux.Vars(r)["id"]
		tea, err := service.Get(bson.ObjectIdHex(id))
		checkError(w, http.StatusBadRequest, err, func() { t.ExecuteTemplate(w, "tea", tea) })
	})
}

func createTeaHandler(w http.ResponseWriter, r *http.Request) {
	checkError(w, http.StatusBadRequest, r.ParseForm(), func() {
		log.Printf("inerted new tea with id: %d", service.SetTea(entity.ParseTea(r.Form)))
		indexHandler(w, r)
	})
}

func editTeaHandler(w http.ResponseWriter, r *http.Request) {
	checkError(w, http.StatusBadRequest, r.ParseForm(), func() {
		checkError(w, http.StatusConflict, service.EditTea(entity.ParseTea(r.Form)), func() {
			indexHandler(w, r)
		})
	})
}

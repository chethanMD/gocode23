package web

import (
	"net/http"
	"html/template"
)

var fm = template.FuncMap{
	"add": add,
}

func parseTemplates(w http.ResponseWriter, handler func(*template.Template)) {
	t := template.New("index").Funcs(fm)
	t, err := t.ParseFiles(filenames...)
	checkError(w, http.StatusInternalServerError, err, func() {
		handler(t)
	})
}

func checkError(w http.ResponseWriter, code int, err error, handler func()) {
	if err != nil {
		writeErrorTemplate(w, err, code)
	} else {
		handler()
	}
}

func add(i int) int {
	return i + 1
}

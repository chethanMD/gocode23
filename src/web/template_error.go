package web

import (
	"net/http"
	"html/template"
	"log"
)

//  path to error templates
const (
	path500 = "res/tmpl/err/500.tmpl"
)

func errorPaths() []string {
	return []string{path500, headPath}
}

func writeErrorTemplate(w http.ResponseWriter, err error, code int) {
	data := struct {
		Code  int
		Error string
	}{code, err.Error()}

	w.WriteHeader(code)

	if t, err := template.ParseFiles(errorPaths()...); err != nil {
		log.Fatal(err.Error())
	} else {
		t.ExecuteTemplate(w, "500", data)
	}

}
